# @name Jacob Gallow, Jon Simington
# @assignment CS3600 Project 3 - RSA Implementation
# @date 11-18-16
# @file p3.py

import random


def gcd(a, b):
    """
    pre: a and b must be integers
    post: Returns the Greatest Common Divisor of a, b using the Euclidian Algorithm
    """

    # While b != 0, continually mod a by b and create new tuple to represent a and b
    # At every iteration i:
    #  a = b(i - 1)
    #  b = a(i - 1) % b(i - 1)
    # continually swaps a and b
    while b != 0:
        (a, b) = (b, a % b)

    # When b == 0 the remainder of a(i - 1) / b(i - 1) == 0 which means the
    # GCD has been found.
    return int(a)


def linear_combination(a, b):
    """
    Returns the following to be formed into an equation ax + by = gcd(a, b):

    g: GCD of a, b
    x: constant multiplier of the x variable in the formula above
    y: constant multiplier of the y variable in the formula above
    """

    # If the user gave a = 0, then the GCD is b, and x = 0, y = 1
    # Return this data as a tuple since there is no calculation to be done
    if a == 0:
        return (b, 0, 1)

    # Otherwise, we need to recursively calculate the linear combination
    else:
        g, y, x = linear_combination(b % a, a)

    # NOTE: // is integer division
    # x = x(i - i) - (b // a) * y(i - 1) where i is an imaginary recursion
    # counter
    x = x - (b // a) * y

    # Finally, we have recursed enough such that ax + by == gcd(a, b) and we can
    # return g, x, y as a tuple
    return (g, x, y)


def multiplicative_inverse(a, b):
    """
    Returns the multiplicative inverse of a and b
    """
    g, x, y = linear_combination(a, b)

    if g != 1:
        raise ValueError

    return x % b


def is_prime(n):
    """
    Returns True if n is prime
    """
    if n % 2 == 0 and n > 2:
        return False
    # goes through all possible numbers to see if n is prime
    return all(n % i for i in range(3, int(n ** (.5)) + 1, 2))


def gen_keypair(p, q, e):
    """
    Takes two (preferrably large) integers, p and q, and returns:
      - public key (e, n)
      - private key (d, n)
    """

    if not (is_prime(p) and is_prime(q)):
        raise ValueError('Please enter two prime numbers.')

    if p == q:
        raise ValueError('p and q must not be equal.')

    n = p * q

    # the result of euler's totient function of n --> phi(n)
    phi = (p - 1) * (q - 1)

    # Find GCD of e and phi, and assert that it is 1.
    # if the GCD of e and phi is not 1, something went wrong and the
    # key generation must stop
    g = gcd(e, phi)
    assert g == 1

    # find the private key, d, which is equal to the multiplicative
    # inverse of e
    d = multiplicative_inverse(e, phi)

    # return keypair
    # public key: (e, n)
    # private key: (d, n)
    return ((e, n), (d, n))


def encrypt(key, plaintext):
    """
    Takes a public key consisting of (e, n), and an integer to be encrypted.
    Returns the resulting ciphertext

    NOTE: this only works for integers, additional processing is required to
    encrypt / decrypt a string, or other data type.
    """

    # Since the key is a tuple, we need to unpack it to access the individual
    # components
    pub_key, n = key

    # equivalent to (text ^ pub_key) % n
    # python's pow function simplifies this
    ciphertext = pow(plaintext, pub_key, n)

    return ciphertext


def decrypt(key, ciphertext):
    """
    Takes a private key consisting of (d, n), and a ciphertext to be decrypted.
    Returns the original, decrypted text
    """

    # unpack private key tuple
    priv_key, n = key

    plaintext = pow(ciphertext, priv_key, n)

    return plaintext


if __name__ == '__main__':
    # get p, q, e from the input file
    p_q_e_filename = input(
        'Enter the name of the input file that contains p, q and e: ')

    with open(p_q_e_filename, 'r') as p_q_e_file:
        p = int(p_q_e_file.readline())
        q = int(p_q_e_file.readline())
        e = int(p_q_e_file.readline())

    # generate a keypair
    keypair = gen_keypair(p, q, e)

    # unpack the keypair and its enclosed tuples to get individual access
    # to the public / private keys
    e, d = keypair
    e, n = e
    d, n = d

    d_n_filename = input('Enter the output file name to store d and N: ')

    with open(d_n_filename, 'w') as keypair_file:
        keypair_file.write(str(d) + '\n')
        keypair_file.write(str(n) + '\n')

    message_filename = input(
        'Enter the name of the file that contains x to be encrypted using (N, e): ')

    with open(message_filename, 'r') as message:
        message = int(message.readline())

    encrypted_output_filename = input('Enter the output file to store E(x): ')

    with open(encrypted_output_filename, 'w') as encrypted_output_file:
        # repack the public key into a tuple, and use it to encrypt the message
        ciphertext = encrypt((e, n), message)
        encrypted_output_file.write(str(ciphertext))

    encrypted_message_filename = input(
        'Enter the name of the file that contains c to be decrypted using d: ')

    with open(encrypted_message_filename, 'r') as encrypted_message_file:
        encrypted_message = int(encrypted_message_file.readline())

    decrypted_filename = input('Enter the output file name to store D(c): ')

    with open(decrypted_filename, 'w') as decrypted_file:
        plain_text = decrypt((d, n), encrypted_message)
        decrypted_file.write(str(plain_text))
