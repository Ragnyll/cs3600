Prerequisites:

Steps to run our submission:

1) Make sure you are on a machine with python 3.x installed by running python --version at a terminal. As long as it returns Python 3.x or greater, proceed to step 2.

2) Run the file with python p3.py. The program will ask you to enter an input file name (please have this file in the project directory or be ready to give it the full path which is a little more work), and to enter the name of the file you want the results printed to.

3) Read output contained in the file with filename you provided.
