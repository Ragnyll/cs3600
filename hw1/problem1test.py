n = int(input("n > 0:\t"))

e = n
o_e = e

y = 1

a = int(input("a != 0:\t"))

z = a

while e > 0:
    if e % 2 == 1:
        y = y * z
    z = z * z
    e = e // 2

print('y: ', y)

print("is y = a ^ z:\t", y == a ** o_e)
