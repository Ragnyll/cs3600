---
title: CS3600 Homework 1
author: Jacob Gallow and Jon Simington
geometry: top=0cm, left=1in, right=1in

---

1.  Describe what Algorithm 1 computes and analyze a tight upper bound regarding its computation complexity in terms of the number of multiplications needs to be performed.$\\$

    This algorithm computes $y = a^e$, where a, and e are their original values.  We were able to verify this with many different inputs using the Python 3 code below.  Please note that we converted your pseudocode given in problem 1 into Python 3 for ease of verification.$\\$

    ```python
    n = int(input("n > 0:\t"))
    e = n

    # store the original value for e so we can verify that our hypothesis of y = a ^ e is true.
    o_e= e
    
    y = 1
    a = int(input("a != 0:\t"))
    z = a

    while e > 0:
    	  if e % 2 == 1:
             y = y * z
    	  z = z * z

   	      # NOTE: in Python 3, the // operator performs integer divsion, which is like taking the 
	      # floor of a decimal.
    	  e = e // 2

    # print what we got for y when e eventually reaches 0
    print('y:\t', y)

    # check to make sure that y is indeed equal to a raised to the power of the original e.
    print("is y = a ^ z:\t", y == a ** o_e)
    ```
    
    $\\$The tight upper bound for computational complexity is $O(log_2(n))$ 

    

---

2. Find the GCD of `512` and `960`, and the linear combination of the GCD in term of `512` and `960`.

### GCD

	Using the Euclidian Algorithm:

    	
        GCD(a, b):
            while b != 0:
                a = b
	        b = a mod b
       
$\\$

        Iteration 0:
        
    	    a = 512, b = 960
        

    	
    	Iteration 1:
    	
	        b = 960 % 512 = 448
    	    a = 512
    	

    	Iteration 2:
    	
	        b = 512 % 448 = 64
	        a = 448
    	
    	Iteration 3:

	        b = 448 % 64 == 0
    	

    	Since b is now zero, we can confidently conclude that we found the GCD --> GCD = b = 64

###Linear Combination

        a = 512 b = 960

        512  % 960 = 448

        448 % 512 = 64

        64 is GCD

	    From here, we are just enumerating some possible constant multipliers for a, b to obtain a 

	    linear combination for the GCD of 512, 960.

        64 = 0a + 1b

        64 = 1a + 0b

        64 = 1a - 1b

        64 = 2a - 1b

the linear combination of the GCD(512, 960) is $2a - 1b = 64$, or $2(512) - 1(960) = 64$

---

$\pagebreak$

$\\$

$\\$

3. Find the multiplicative inverse of 37 in Z_157 if exists.
   
        x_3 = x_1 + x_2 mod N (= Zn
   
	verifying that a multiplicative inverse exists:

   	37 % 157 = 1
   
	--> a multiplicative inverse exists
   
    ~~~
    for x = 1 upto 157
        if (37 * x ) % 157 == 1
            x is the gcd
    ~~~

	We turned the pseudocode above into python code to obtain the following answer, since doing it by hand would have taken a long time:

   	X = 17

---

4. If a, b, c, d and m are integers with m > 0, a === b mod m and c === d mod m, prove the following statements hold:
        
    The following are true in the proofs below:

    `a = b + mq`, where q is some integer

    `c = d + mr` where r is some integer

$\\$

- a – c $\equiv$ b – d mod m

    We want to show m | [(a - c) + (b - d)]:

        (a - c) + (b - d) = (b - mq + d - mr) + (-b - d)

        (a - c) + (b - d) = b - b + d - d - mq - mr

	(a - c) + (b - d) = mq - mr

	(a - c) + (b - d) = m(q + r) 
		--> m | m(q - r) 
		QED.


$\\$

- ac $\equiv$ bd mod m

    We want to show m | (ac - bd):

        ac - bd = ((qm + b) * c) - (b * (c-rm))
    
        ac - bd = qmc + bc - bc + rmb

        ac - bd = qmc + rmb

        ac - bd = m(qc + rb) 
        
        	-->  m | m(qc + rb) 
        	QED.


