Steps to run our submission:

1) Make sure you are on a machine with python installed by running `python --version` at a terminal.  As long as it returns `Python 3.5.x` or greater, proceed to step 2.

2) Try running the file with `python3 project1.py`, and if that doesn't work, use `python project1.py`. If neither of these work then you may want check your system PATH
variable and make sure you have python 3.5 installed on your system. 

3) Follow onscreen instructions

4) Read output
