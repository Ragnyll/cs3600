# @name Jacob Gallow, Jon Simington
# @assignment CS3600 Project 1
# @date 10-5-16
# @file project1.py

import sys


def gcd(a, b):
    """
    pre: a and b must be integers
    post: Returns the Greatest Common Divisor of a, b using the Euclidian Algorithm
    """

    # While b != 0, continually mod a by b and create new tuple to represent a and b
    # At every iteration i:
    #  a = b(i - 1)
    #  b = a(i - 1) % b(i - 1)
    # continually swaps a and b
    while b != 0:
        (a, b) = (b, a % b)

    # When b == 0 the remainder of a(i - 1) / b(i - 1) == 0 which means the
    # GCD has been found.
    return int(a)


def linear_combination(a, b):
    """
    pre: a and b must be integers
    post: Returns the following to be formed into an equation ax + by = gcd(a, b):

    g: GCD of a, b
    x: constant multiplier of the x variable in the formula above
    y: constant multiplier of the y variable in the formula above
    """

    # If the user gave a = 0, then the GCD is b, and x = 0, y = 1
    # Return this data as a tuple since there is no calculation to be done
    if a == 0:
        (g, x, y) = (b, 0, 1)
        return (g, x, y)

    # Otherwise, we need to recursively calculate the linear combination
    else:
        g, y, x = linear_combination(b % a, a)
    # NOTE: // is integer division
    # x = x(i - i) - (b // a) * y(i - 1) where i is an imaginary recursion counter
    # print a, b
    x = x - (b // a) * y

    # Finally, we have recursed enough such that ax + by == gcd(a, b) and we can
    # return g, x, y as a triple
    # print g, x, y
    return (g, x, y)


# Prompt user for input when they run the script normally, and report the GCD
# and linear combination of a and b, given by the user
if __name__ == "__main__":
    # tests to make sure everything is working before user puts numbers in
    assert gcd(720, 480) == 240
    assert linear_combination(720, 480) == (240, 1, -1)

    # Auxiliary printing
    print('-' * 40)

    # Receieve input from user
    a = int(input("Enter an integer value for a:  "))
    b = int(input("Enter an integer value for b:  "))

    # Calculate GCD
    GCD = gcd(a, b)

    # Calculate constants (x, y) and GCD(a, b) for linear combination
    l_c_g, l_c_x, l_c_y = linear_combination(a, b)

    # Print out GCD
    print("The GCD of a and b is:" + str(GCD))

    # Print and format the linear combination
    if l_c_y > 0:
        print("The linear combination of GCD(a, b) is:" +
              str(l_c_x) + "a + " + str(l_c_y) + "b = " + str(l_c_g))

    # NOTE: There is some multiplication by -1, but that is only to show a pretty printout of
    # ax (+|-) by = gcd(a,b).  THE NUMBERS ARE CORRECT!
    else:
        print("The linear combination of GCD(a, b) is:" + str(l_c_x) +
              "a - " + str(l_c_y * -1) + "b = " + str(l_c_g))

    # Auxiliary printing
    print('-' * 40)
