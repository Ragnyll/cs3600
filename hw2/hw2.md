---
title: CS3600 Homework 2
author: Jon Simington
geometry: top=2cm, left=1in, right=1in

---

1.  Explain how Diffe-Hellman key exchange protocol works and why it is secure.

---


---

2.  Suppose `n = pq` where `p` and `q` are prime numbers.

* Explain the meaning of the Euler's totient function $\phi$

* Prove that $\phi$ (n) = (p - 1)(q - 1)

---

---

3.  Without using a computer or calculator, compute 8^183 mod 77

---

---

4.  let `p = 11` and `q = 13`.  Suppose `e = 7` is the public key in RSA:

* Find the private key d

* Given `x = 5`, verify if `D(E(x)) = 5` where `E` and `D` are the encryption and decryption functions with public key `e` and private key `d` respectively.

---

---

5.  Solve the linear congruence `63x \equiv 45 mod 270`

---
