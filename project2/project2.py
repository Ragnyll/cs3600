# @name Jacob Gallow, Jon Simington
# @assignment CS3600 Project 2
# @date 11-4-15
# @file project2.py

import sys


def gcd(a, b):
    """
    Returns the Greatest Common Divisor of a, b using the Euclidian Algorithm
    """

    # While b != 0, continually mod a by b and create new tuple to represent a and b
    # At every iteration i:
    #  a = b(i - 1)
    #  b = a(i - 1) % b(i - 1)
    while b != 0:
        (a, b) = (b, a % b)

    # When b == 0 the remainder of a(i - 1) / b(i - 1) == 0 which means the
    # GCD has been found.
    return int(a)


def linear_combination(a, b):
    """
    Returns the following to be formed into an equation ax + by = gcd(a, b):

    g: GCD of a, b
    x: constant multiplier of the x variable in the formula above
    y: constant multiplier of the y variable in the formula above
    """

    # If the user gave a = 0, then the GCD is b, and x = 0, y = 1
    # Return this data as a tuple since there is no calculation to be done
    if a == 0:
        return (b, 0, 1)

    # Otherwise, we need to recursively calculate the linear combination
    else:
        g, y, x = linear_combination(b % a, a)

    # NOTE: // is integer division
    # x = x(i - i) - (b // a) * y(i - 1) where i is an imaginary recursion
    # counter
    x = x - (b // a) * y

    # Finally, we have recursed enough such that ax + by == gcd(a, b) and we can
    # return g, x, y as a tuple
    return (g, x, y)


# Prompt user for input when they run the script normally, and report the GCD
# and linear combination of a and b, given by the user
if __name__ == "__main__":
    # tests to make sure everything is working before user puts numbers in
    assert gcd(720, 480) == 240
    assert linear_combination(720, 480) == (240, 1, -1)

    # Prompt user for input / output file names
    input_file_name = input("Please enter the file name of the input file:  ")
    output_file_name = input(
        "What would you like the output file to be called?  ")

    # Open input file
    with open(input_file_name, "r") as f:

        # Tell the user that the file is open
        print("Opened " + input_file_name + " in read only mode")

        # Auxiliary printing
        print('-' * 40)

        # Receive input from file
        print("READING FROM " + input_file_name.upper() + " FOR A")
        a = int(f.readline())
        print("READING FROM " + input_file_name.upper() + " FOR B")
        b = int(f.readline())

        # Calculate GCD
        GCD = gcd(a, b)

        # Calculate constants (x, y) and GCD(a, b) for linear combination
        l_c_g, l_c_x, l_c_y = linear_combination(a, b)

        # Write to output file as desired by user
        output_file = open(output_file_name, 'w+')

        # Write GCD to first line
        output_file.write(str(l_c_g) + "\n")

        # Write coefficient of A to second line
        output_file.write(str(abs(l_c_x)) + "\n")

        # Write coefficient of B to third line
        output_file.write(str(abs(l_c_y)) + "\n")

        # Close file stream
        output_file.close()

        # Auxiliary printing
        print('-' * 40)

        print("Wrote GCD and linear combination of a, b provided in {} to {}".format(
            input_file_name, output_file_name))
